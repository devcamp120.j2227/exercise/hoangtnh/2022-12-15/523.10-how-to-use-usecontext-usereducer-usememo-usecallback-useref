import { useContext } from "react"
import ChildComponent2 from "./ChildComponent2";
import { ExampleContext } from "./ExampleContext"

function ChildComponent1() {
    const valueFromContext = useContext(ExampleContext);
    return (
        <div style={{display:"none"}}>
            <h1>{valueFromContext.firstValue}</h1>
            <ChildComponent2/>
        </div>
    )
}

export default ChildComponent1