import React, { useState, useEffect, useCallback } from "react";

const ChildComponent = ({ loggingStatus }) => {
  useEffect(() => {
    loggingStatus();
  }, [loggingStatus]);
  return <div />;
};

const UseCallbackComponent = () => {
  const [count, setCount] = useState(0);
  const loggingStatus = useCallback(() => {
    console.log("Run from ChildComponent");
  }, []);
  // const loggingStatus = () => {
  //   console.log("Run from ChildComponent");
  // };  
  const addMore = () => {
    setCount((prev) => prev + 1);
  };

  return (
    <div>
      <p>Current: {count}</p>
      <ChildComponent loggingStatus={loggingStatus} />
      <button onClick={addMore}>Click</button>
    </div>
  );
};

export default UseCallbackComponent;
