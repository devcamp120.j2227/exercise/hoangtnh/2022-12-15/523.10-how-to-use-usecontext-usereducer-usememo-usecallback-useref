import React, { useState, useMemo } from "react";

const UseMemoComponent = () => {
  const [text, setText] = useState("Hello!");
  const [count, setCount] = useState(0);

  const ChildComponent = ({ text }) => {
    console.log("rendered again!");
    return <div>{text}</div>;
  };

  const MemoizedComponent = useMemo(() => <ChildComponent text={text} />, [
    text
  ]);
  return (
    <div style={{display:"none"}}>      
      <button onClick={() => setText("Hello!")}>Hello! </button>
      <button onClick={() => setText("Hola!")}>Hola!</button>
      <button onClick={() => setCount(count + 1)}>Increase</button>
      {MemoizedComponent}<br/>
      Count: {count}
    </div>
  );
};

export default UseMemoComponent;
